package com.xdome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class XdomeApplication {

	public static void main(String args[]) {
		SpringApplication.run(XdomeApplication.class);
	}
}
