package com.xdome.config;
 
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
 
@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.xdome.domain"})
@EnableJpaRepositories(basePackages = {"com.xdome.repository"})
@EnableTransactionManagement
public class RepositoryConfiguration {
}