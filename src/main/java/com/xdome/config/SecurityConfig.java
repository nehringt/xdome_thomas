package com.xdome.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		/* Authorizes all http requests. Without this, we get a login prompt.
		 * TODO: Add authentication/authorization.
		 */
		http.authorizeRequests().antMatchers("/**").permitAll();
	}

}

