/**
 * 
 */
package com.xdome.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xdome.service.ArticleService;
import com.xdome.service.CommentBlockService;

/**
 * @author steve_peralta
 *
 */
@Controller
public class IndexController {

	/**
	 * 
	 */
	private ArticleService articleService;
	private CommentBlockService commentBlockService;
	
	@Autowired
	public IndexController(ArticleService articleService, CommentBlockService commentBlockService) {
		this.articleService = articleService;
		this.commentBlockService = commentBlockService;
	}
	
	 @RequestMapping("/")
	 public String index(Model model){
		 model.addAttribute("commentBlocks", commentBlockService.getAllCommentBlocks());
		 model.addAttribute("articles", articleService.getAllArticles());
	    return "index";
	}


}
