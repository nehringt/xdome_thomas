/**
 * 
 */
package com.xdome.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author steve_peralta
 *
 */
@Entity
public class Article {
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer primaryKey;
	private String label;
	private String name;
	private Integer fkTitleDivision;
	private Integer fkTitle;

	/**
	 * @return the primaryKey
	 */
	@Id
	public Integer getPrimaryKey() {
		return primaryKey;
	}
	/**
	 * @param primaryKey the primaryKey to set
	 */
	public void setPrimaryKey(Integer primaryKey) {
		this.primaryKey = primaryKey;
	}
	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the fkTitleDivision
	 */
	public Integer getFkTitleDivision() {
		return fkTitleDivision;
	}
	/**
	 * @param fkTitleDivision the fkTitleDivision to set
	 */
	public void setFkTitleDivision(Integer fkTitleDivision) {
		this.fkTitleDivision = fkTitleDivision;
	}
	/**
	 * 
	 */
	public Integer getFkTitle() {
		return fkTitle;
	}
	public void setFkTitle(Integer fkTitle) {
		this.fkTitle = fkTitle;
	}
	
	
	
	@Override
	public String toString() {
		return "Article [PrimaryKey=" + primaryKey + ", Label=" + label + ", Name=" + name + ", fkTitleDivision="
				+ fkTitleDivision + "]";
	}
	
	

}
