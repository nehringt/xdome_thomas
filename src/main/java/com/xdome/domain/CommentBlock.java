/**
 * 
 */
package com.xdome.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author steve_peralta
 *
 */
@Entity
public class CommentBlock {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer primaryKey;
	private String type;
	private String name;
	private String text;
	private Integer fkArticle;
	
	/**
	 * @return the primaryKey
	 */
	public Integer getPrimaryKey() {
		return primaryKey;
	}
	/**
	 * @param primaryKey the primaryKey to set
	 */
	public void setPrimaryKey(Integer primaryKey) {
		this.primaryKey = primaryKey;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return the fkArticle
	 */
	public Integer getFkArticle() {
		return fkArticle;
	}
	/**
	 * @param fkArticle the fkArticle to set
	 */
	public void setFkArticle(Integer fkArticle) {
		this.fkArticle = fkArticle;
	}
	
	
}