package com.xdome.repository;

/**
 * @author steve_peralta
 *
 */

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.xdome.domain.Article;

@Repository
public interface ArticleRepository extends CrudRepository<Article, Integer> {
	List<Article> findAll();
}
