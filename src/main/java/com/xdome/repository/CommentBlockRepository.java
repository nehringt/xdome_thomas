package com.xdome.repository;

/**
 * @author steve_peralta
 *
 */

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.xdome.domain.CommentBlock;

@Repository
public interface CommentBlockRepository extends CrudRepository<CommentBlock, Integer> {
	List<CommentBlock> findAll();
}
