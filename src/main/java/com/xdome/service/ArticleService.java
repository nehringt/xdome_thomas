package com.xdome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.xdome.domain.Article;

@Service
public interface ArticleService {
	public List <Article> getAllArticles();
}