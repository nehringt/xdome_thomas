package com.xdome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.xdome.domain.CommentBlock;

@Service
public interface CommentBlockService {
	public List <CommentBlock> getAllCommentBlocks();
}