package com.xdome.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xdome.domain.Article;
import com.xdome.repository.ArticleRepository;
import com.xdome.service.ArticleService;

@Service
public class ArticleServiceImpl implements ArticleService{
	
	private ArticleRepository articleRepository;
	
	@Autowired
	public ArticleServiceImpl(ArticleRepository articleRepository){
		this.articleRepository = articleRepository;
	}
	
	public List<Article> getAllArticles() {
		return articleRepository.findAll();
	}
	
}