package com.xdome.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xdome.domain.CommentBlock;
import com.xdome.repository.CommentBlockRepository;
import com.xdome.service.CommentBlockService;

@Service
public class CommentBlockServiceImpl implements CommentBlockService{
	
	private CommentBlockRepository commentBlockRepository;
	
	@Autowired
	public CommentBlockServiceImpl(CommentBlockRepository commentBlockRepository){
		this.commentBlockRepository = commentBlockRepository;
	}
	
	public List<CommentBlock> getAllCommentBlocks() {
		return commentBlockRepository.findAll();
	}
	
}