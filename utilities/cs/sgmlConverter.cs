using System;
using System.Web.UI;

//Import this DLL!
using Sgml;

using System.Xml;
using System.IO;

namespace sgmlConverter
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //Get files in directory
            string[] filePaths = Directory.GetFiles(@"C:\sgml\Title20", "*.*");

            //Iterate through each file, convert to XML, save newly created XML file
            foreach (string filepath in filePaths)
            {
                string sgmlFile = @"C:\sgml\Title20\" + filepath.Remove(0, 16);
                string xmlFile = @"C:\sgml\Title20\xml\" + filepath.Remove(0, 16) + ".xml";

                //Get SGML using System IO
                TextReader sgmlData = new StreamReader(sgmlFile);

                try
                {
                    //Convert to XML from SGML
                    fromSgml(sgmlData, xmlFile);

                } catch(Exception error)

                {
                    Console.WriteLine("{0} Exception caught.", error);
                }

                //Or use System.IO to get info (not accurate)
                //FileInfo fi = new FileInfo(filepath);

                sgmlFile = "";
                xmlFile = "";
            }


        }


        XmlDocument fromSgml(TextReader reader, string xmlPath)
        {

            
            // setup SgmlReader
            SgmlReader sgmlReader = new SgmlReader();
            sgmlReader.DocType = "XML";
            sgmlReader.WhitespaceHandling = WhitespaceHandling.All;
            sgmlReader.CaseFolding = CaseFolding.ToLower;
            sgmlReader.InputStream = reader;
            //Need this DTD or it won't convert!
            sgmlReader.SystemLiteral = "file:///C:/sgml/statutes.dtd";

            // create document
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            doc.XmlResolver = null;
            doc.Load(sgmlReader);

            //Create an XML declaration. 
            XmlDeclaration xmldecl;
            xmldecl = doc.CreateXmlDeclaration("1.0", null, null);

            //Add the XML declaration to the document.
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmldecl, root);

            doc.Save(xmlPath);

            sgmlReader.Close();

            

            return null;

        }

    }
}